**C++ Code Generator**

*Usage*
1. Open the solution in the VS folder in Visual Studio 2015.
2. Build and run CppFuzzer, and C++ code will be generated into test projects (repositoryRoot\Test).
The same translation units will be put into CLion and VS14 test projects as _Code0.cpp_.
To generate more source code files just fix the Program.cs in CppFuzzer project.
Sometimes the process of code generation reaches the state when it isn't able to stop.
In this case (when the number of nodes of syntax tree which are not expanded is more than maximum count) we use some heuristics to end the code generation.
The maximum number of non expanded nodes can be modified - it's `maximumNotExpandedNodesNumber` in _Generator.cs_ (CodeGeneration assembly).
3. There is a tool to generate code and browse its syntax tree. It's not a masterpiece of design, but you can navigate on the syntax tree with this tool.
4. You can use Tester for automatic testing GCC and clang-3.8. In the working directory this tester will create directory with results. It will also create temporary file code.cpp.


*Developing*
There are two python scripts: _GenerateGraph.py_ and _GenerateCode.py_ which are obviously generate respectively the graph and the C# code for the grammar from _grammar.xml_.
The graph is very convenient for grammar browsing - with some graph viewer such as Yed we are can find unused nonterminal symbols in the grammar.
The C# code which generates by _GenerateCode.py_ is a set of classes which are generate C++ syntax tree. If you want to customize some class (add a work with a context for instance) - add `custom="true"` attribute to its "symbol" tag in the _grammar.xml_.
To run the GenerateCode.py you should pass to the first argument the path to the directory where C# classes will be put after the generation - in my case it's ..\VS\CodeGeneration\Symbols\ .
You can change the `factory` in _Generator.cs_ to set the tree root - it's convenient if you want to see which code can be generated for some part of language (for instance, expressions).
You can add mutations into code generation - data and flags for mutations should be added to _Context.cs_. You need also customize some nodes to handle new mutations.