﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Tester {
    static class TesterMain {
        private static string _codeFilename = @"code.cpp";
        private static string _workingDirectory = Directory.GetCurrentDirectory() + "\\";
        private static string _outputPath;

        private static int _treeSize;
        private static int _filesCount;

        private static int _counter;

        private static void TestGcc() {
            var process = new Process {
                StartInfo = {
                    FileName = "gcc",
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    Arguments =
                        "-std=c++14 " + _workingDirectory + _codeFilename + " -o " + _workingDirectory + "code.exe"
                }
            };
            process.Start();

            var reader = process.StandardError;
            var output = reader.ReadToEnd();
            process.WaitForExit();
            process.Close();

            if (output.Contains("internal compiler error")) {
                File.Copy(_workingDirectory + _codeFilename,
                        _outputPath + _counter++ + ".cpp");
            }
        }

        private static void TestClang() {
            var process = new Process {
                StartInfo = {
                    FileName = "clang-3.8.exe",
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    Arguments = "-std=c++14 " + _workingDirectory + _codeFilename + " -o " + _workingDirectory + "code.exe"
                }
            };
            process.Start();

            var reader = process.StandardError;
            var output = reader.ReadToEnd();
            process.WaitForExit();
            process.Close();

            if (output.Contains(" bug ")) {
                File.Copy(_workingDirectory + _codeFilename,
                        _outputPath + _counter++ + ".cpp");
            }
        }

        private static void Main() {
            Console.WriteLine("1 - GCC\n2 - Clang\nEnter choise");
            var choise = Convert.ToInt32(Console.ReadLine());
            if (choise != 1 && choise != 2) {
                Console.WriteLine("Invalid output\n");
                return;
            }
            Console.Write("Enter the number of syntax nodes: ");
            _treeSize = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the number of files for testing: ");
            _filesCount = Convert.ToInt32(Console.ReadLine());

            var outputDirectory =
                    new DirectoryInfo(_workingDirectory +
                                      string.Format("\\output_{0}{1}{2}_{3}{4}{5}", DateTime.Now.Year,
                                              DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour,
                                              DateTime.Now.Minute, DateTime.Now.Second));
            outputDirectory.Create();
            _outputPath = outputDirectory.FullName + "\\";

            for (int i = 0; i < _filesCount; ++i) {
                Console.WriteLine(i);
                using (var writer = new StreamWriter(_workingDirectory + _codeFilename)) {
                    writer.Write(CodeGeneration.Generator.GenerateTree(_treeSize).Serialize());
                }

                switch (choise) {
                    case 1:
                        TestGcc();
                        break;
                    case 2:
                        TestClang();
                        break;
                }    
            }
        }
    }
}
