﻿using System;

namespace CodeGeneration {
    static class Utils {
        private static readonly Random _random = new Random();

        public static int RandomInt() {
            return _random.Next();
        }

        public static int RandomInt(int range) {
            return _random.Next() % range;
        }

        public static int RandomInt(int a, int b) {
            return _random.Next() % (b - a) + a;
        }

        public static double RandomDouble() {
            return _random.NextDouble();
        }

        public static bool RandomBool() {
            return _random.Next() % 2 == 1;
        }

        public static string Repeat(this string src, int count) {
            var result = "";
            for (int i = 0; i < count; ++i) {
                result += src;
            }
            return result;
        }
    }
}
