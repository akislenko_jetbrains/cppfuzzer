﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeGeneration.Symbols;
using CodeGeneration.Symbols.BasicConcepts;
using CodeGeneration.Symbols.Classes;
using CodeGeneration.Symbols.Declarations;
using CodeGeneration.Symbols.Declarators;
using CodeGeneration.Symbols.DerivedClasses;
using CodeGeneration.Symbols.Expressions;
using CodeGeneration.Symbols.LexicalConventions;
using CodeGeneration.Symbols.Overloading;
using CodeGeneration.Symbols.Statements;
using CodeGeneration.Symbols.Templates;

namespace CodeGeneration {
    public static class Generator {
        public static ISymbol GenerateTree(int maximumNotExpandedNodesNumber = 100) {
            var mutation = new Mutation {RandomDeletionProbability = 0.2 / maximumNotExpandedNodesNumber};
            var context = new Context(mutation);

            var factory = new TranslationUnitFactory();
            var notExpanded = new List<INonterminalSymbol>();

            var rootMaybeTerminal = factory.CreateInstance(context);
            var root = rootMaybeTerminal as INonterminalSymbol;
            if (root == null) {
                return rootMaybeTerminal;
            }

            notExpanded.Add(root);

            while (notExpanded.Count > 0 && notExpanded.Count < maximumNotExpandedNodesNumber) {
                var index = Utils.RandomInt(notExpanded.Count);
                var symbol = notExpanded[index];
                var expansionResult = symbol.Expand(false, context);

                if (expansionResult.Item2) {
                    notExpanded[index] = notExpanded.Last();
                    notExpanded.RemoveAt(notExpanded.Count - 1);
                    notExpanded.AddRange(expansionResult.Item1);
                }
            }

            while (notExpanded.Count > 0) {
                var newNodes = new List<INonterminalSymbol>();
                foreach (var symbol in notExpanded) {
                    var expansionResult = symbol.Expand(true, context);
                    if (expansionResult.Item2) {
                        newNodes.AddRange(expansionResult.Item1);
                    } else {
                        newNodes.Add(symbol);
                    }
                }

                if (notExpanded == newNodes) {
                    throw new Exception("There are no nodes that are able to be expanded.");
                }
                notExpanded = newNodes;
            }

            return root;
        }
    }
}