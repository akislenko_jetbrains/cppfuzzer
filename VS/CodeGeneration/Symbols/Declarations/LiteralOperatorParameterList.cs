using System;
using System.Collections.Generic;
using System.Linq;
using CodeGeneration.Symbols.BasicConcepts;
using CodeGeneration.Symbols.Classes;
using CodeGeneration.Symbols.Declarations;
using CodeGeneration.Symbols.Declarators;
using CodeGeneration.Symbols.DerivedClasses;
using CodeGeneration.Symbols.ExceptionHandling;
using CodeGeneration.Symbols.Expressions;
using CodeGeneration.Symbols.LexicalConventions;
using CodeGeneration.Symbols.Overloading;
using CodeGeneration.Symbols.SpecialMemberFunctions;
using CodeGeneration.Symbols.Statements;
using CodeGeneration.Symbols.Templates;

namespace CodeGeneration.Symbols.Declarations {
    class LiteralOperatorParameterListFactory : ISymbolFactory {
        private readonly double  _optional;

        public LiteralOperatorParameterListFactory(double optional = 1) {
            _optional = optional;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() < context.Mutation.RandomDeletionProbability) {
                return new TerminalSymbol(@"/*tree corruption*/");
            }
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new LiteralOperatorParameterList();
        }
    }

    class LiteralOperatorParameterList : INonterminalSymbol {
        private bool _expanded;
        private readonly List<ISymbol> _symbols = new List<ISymbol>();
        private static readonly ISymbolFactory[] DefaultRule = {new TerminalSymbolFactory("const char*"), new IdentifierFactory(optional: 0.9), };

        private static readonly Tuple<ISymbolFactory[], double>[] Rules = {
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("const char*"), new IdentifierFactory(optional: 0.9), }, 0.09090909090909091), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("unsigned long long int"), new IdentifierFactory(optional: 0.9), }, 0.18181818181818182), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("long double"), new IdentifierFactory(optional: 0.9), }, 0.2727272727272727), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("char"), new IdentifierFactory(optional: 0.9), }, 0.36363636363636365), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("wchar_t"), new IdentifierFactory(optional: 0.9), }, 0.45454545454545453), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("char16_t"), new IdentifierFactory(optional: 0.9), }, 0.5454545454545454), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("char32_t"), new IdentifierFactory(optional: 0.9), }, 0.6363636363636364), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("const char*"), new IdentifierFactory(optional: 0.9), new TerminalSymbolFactory(", unsigned long long int"), new IdentifierFactory(optional: 0.9), }, 0.7272727272727273), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("const wchar_t*"), new IdentifierFactory(optional: 0.9), new TerminalSymbolFactory(", unsigned long long int"), new IdentifierFactory(optional: 0.9), }, 0.8181818181818182), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("const char16_t"), new IdentifierFactory(optional: 0.9), new TerminalSymbolFactory(", unsigned long long int"), new IdentifierFactory(optional: 0.9), }, 0.9090909090909091), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("const char32_t"), new IdentifierFactory(optional: 0.9), new TerminalSymbolFactory(", unsigned long long int"), new IdentifierFactory(optional: 0.9), }, 1.0), 
        };

        public string Serialize() {
            return _expanded ? SymbolUtils.Serialize(_symbols) : "[" + GetType().Name + "]";
        }

        public List<ISymbol> Children() {
            return _symbols;
        }

        public Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context) {
            _symbols.Clear();
            _expanded = true;

            var chosenRule = DefaultRule;
            if (!timeToExit) {
                var selector = Utils.RandomDouble();
                foreach (Tuple<ISymbolFactory[], double> rule in Rules) {
                    if (selector < rule.Item2) {
                        chosenRule = rule.Item1;
                        break;
                    }
                }
            }

            return new Tuple<List<INonterminalSymbol>, bool>(SymbolUtils.Expand(chosenRule, _symbols, context), true);
        }
    }
}