using System;
using System.Collections.Generic;
using System.Linq;
using CodeGeneration.Symbols.BasicConcepts;
using CodeGeneration.Symbols.Classes;
using CodeGeneration.Symbols.Declarations;
using CodeGeneration.Symbols.Declarators;
using CodeGeneration.Symbols.DerivedClasses;
using CodeGeneration.Symbols.ExceptionHandling;
using CodeGeneration.Symbols.Expressions;
using CodeGeneration.Symbols.LexicalConventions;
using CodeGeneration.Symbols.Overloading;
using CodeGeneration.Symbols.SpecialMemberFunctions;
using CodeGeneration.Symbols.Statements;
using CodeGeneration.Symbols.Templates;

namespace CodeGeneration.Symbols.Classes {
    class MemberFieldDeclarationBlockFactory : ISymbolFactory {
        private readonly double  _optional;

        public MemberFieldDeclarationBlockFactory(double optional = 1) {
            _optional = optional;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() < context.Mutation.RandomDeletionProbability) {
                return new TerminalSymbol(@"/*tree corruption*/");
            }
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new MemberFieldDeclarationBlock();
        }
    }

    class MemberFieldDeclarationBlock : INonterminalSymbol {
        private bool _expanded;
        private readonly List<ISymbol> _symbols = new List<ISymbol>();
        private static readonly ISymbolFactory[] DefaultRule = {new TerminalSymbolFactory("/*member field declaration block*/"), };

        private static readonly Tuple<ISymbolFactory[], double>[] Rules = {
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("static"), new NonConstTypeIdFactory(), new FieldDeclarationWithoutInitSeqFactory(optional: 0.5), new FieldDeclarationWithoutInitFactory(), new TerminalSymbolFactory(";"), }, 0.16666666666666666), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("static"), new MultiVariantTerminalSymbolFactory(new[] {"const", "constexpr", }), new NonConstTypeIdFactory(), new FieldDeclarationSeqFactory(optional: 0.5), new FieldDeclarationFactory(), new TerminalSymbolFactory(";"), }, 0.3333333333333333), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("const"), new NonConstTypeIdFactory(), new FieldDeclarationWithInitSeqFactory(optional: 0.5), new FieldDeclarationWithInitFactory(), new TerminalSymbolFactory(";"), }, 0.5), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new NonConstTypeIdFactory(), new FieldDeclarationSeqFactory(optional: 0.5), new FieldDeclarationFactory(), new TerminalSymbolFactory(";"), }, 0.6666666666666666), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("volatile", optional: 0.5), new IntegralTypeFactory(), new IntegralFieldDeclarationSeqFactory(optional: 0.5), new IntegralFieldDeclarationFactory(), new TerminalSymbolFactory(";"), }, 0.8333333333333334), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("mutable"), new NonConstTypeIdFactory(), new FieldDeclarationSeqFactory(optional: 0.5), new FieldDeclarationFactory(), new TerminalSymbolFactory(";"), }, 1.0), 
        };

        public string Serialize() {
            return _expanded ? SymbolUtils.Serialize(_symbols) : "[" + GetType().Name + "]";
        }

        public List<ISymbol> Children() {
            return _symbols;
        }

        public Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context) {
            _symbols.Clear();
            _expanded = true;

            var chosenRule = DefaultRule;
            if (!timeToExit) {
                var selector = Utils.RandomDouble();
                foreach (Tuple<ISymbolFactory[], double> rule in Rules) {
                    if (selector < rule.Item2) {
                        chosenRule = rule.Item1;
                        break;
                    }
                }
            }

            return new Tuple<List<INonterminalSymbol>, bool>(SymbolUtils.Expand(chosenRule, _symbols, context), true);
        }
    }
}