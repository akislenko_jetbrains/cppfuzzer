﻿using System.Collections.Generic;
using System.Linq;

namespace CodeGeneration.Symbols {
    static class SymbolUtils {
        public static string Serialize(List<ISymbol> symbols) {
            return symbols.Aggregate("", (current, symbol) => current + symbol.Serialize());
        }

        public static List<INonterminalSymbol> Expand(ISymbolFactory[] rule, List<ISymbol> symbols, Context context) {
            var result = new List<INonterminalSymbol>();

            foreach (var factory in rule) {
                var newSymbol = factory.CreateInstance(context);
                symbols.Add(newSymbol);
                
                var castedSymbol = newSymbol as INonterminalSymbol;
                if (castedSymbol != null) {
                    result.Add(castedSymbol);
                }
            }

            return result;
        }
    }
}
