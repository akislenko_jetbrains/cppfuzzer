using System;
using System.Collections.Generic;
using System.Linq;
using CodeGeneration.Symbols.BasicConcepts;
using CodeGeneration.Symbols.Classes;
using CodeGeneration.Symbols.Declarations;
using CodeGeneration.Symbols.Declarators;
using CodeGeneration.Symbols.DerivedClasses;
using CodeGeneration.Symbols.ExceptionHandling;
using CodeGeneration.Symbols.Expressions;
using CodeGeneration.Symbols.LexicalConventions;
using CodeGeneration.Symbols.Overloading;
using CodeGeneration.Symbols.SpecialMemberFunctions;
using CodeGeneration.Symbols.Statements;
using CodeGeneration.Symbols.Templates;

namespace CodeGeneration.Symbols.LexicalConventions {
    class LiteralFactory : ISymbolFactory {
        private readonly double  _optional;

        public LiteralFactory(double optional = 1) {
            _optional = optional;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() < context.Mutation.RandomDeletionProbability) {
                return new TerminalSymbol(@"/*tree corruption*/");
            }
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new Literal();
        }
    }

    class Literal : INonterminalSymbol {
        private bool _expanded;
        private readonly List<ISymbol> _symbols = new List<ISymbol>();
        private static readonly ISymbolFactory[] DefaultRule = {new IntegerLiteralFactory(), };

        private static readonly Tuple<ISymbolFactory[], double>[] Rules = {
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new IntegerLiteralFactory(), }, 0.14285714285714285), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new CharacterLiteralFactory(), }, 0.2857142857142857), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new FloatingLiteralFactory(), }, 0.42857142857142855), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new StringLiteralFactory(), }, 0.5714285714285714), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new BooleanLiteralFactory(), }, 0.7142857142857143), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PointerLiteralFactory(), }, 0.8571428571428571), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new UserDefinedLiteralFactory(), }, 1.0), 
        };

        public string Serialize() {
            return _expanded ? SymbolUtils.Serialize(_symbols) : "[" + GetType().Name + "]";
        }

        public List<ISymbol> Children() {
            return _symbols;
        }

        public Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context) {
            _symbols.Clear();
            _expanded = true;

            var chosenRule = DefaultRule;
            if (!timeToExit) {
                var selector = Utils.RandomDouble();
                foreach (Tuple<ISymbolFactory[], double> rule in Rules) {
                    if (selector < rule.Item2) {
                        chosenRule = rule.Item1;
                        break;
                    }
                }
            }

            return new Tuple<List<INonterminalSymbol>, bool>(SymbolUtils.Expand(chosenRule, _symbols, context), true);
        }
    }
}