﻿using System.Collections.Generic;
using System.Linq;

namespace CodeGeneration.Symbols {
    interface ISymbolFactory {
        ISymbol CreateInstance(Context context);
    }

    class TerminalSymbolFactory : ISymbolFactory {
        private readonly double _optional;
        private readonly string _value;
        private readonly int _leftIndent;
        private readonly int _rightIndent;

        public TerminalSymbolFactory(string value, double optional = 1, int leftIndent = 1, int rightIndent = 1) {
            _value = value;
            _optional = optional;
            _leftIndent = leftIndent;
            _rightIndent = rightIndent;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new TerminalSymbol(" ".Repeat(_leftIndent) + _value + " ".Repeat(_rightIndent));
        }
    }

    class MultiVariantTerminalSymbolFactory : ISymbolFactory {
        private readonly double _optional;
        private readonly string[] _values;
        private readonly int _leftIndent;
        private readonly int _rightIndent;

        public MultiVariantTerminalSymbolFactory(IEnumerable<string> values, double optional = 1, int leftIndent = 1,
            int rightIndent = 1) {
            _values = values.ToArray();
            _optional = optional;
            _leftIndent = leftIndent;
            _rightIndent = rightIndent;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return
                new TerminalSymbol(" ".Repeat(_leftIndent) + _values[Utils.RandomInt(_values.Length)] +
                                   " ".Repeat(_rightIndent));
        }
    }
}
