using System;
using System.Collections.Generic;
using System.Linq;
using CodeGeneration.Symbols.BasicConcepts;
using CodeGeneration.Symbols.Classes;
using CodeGeneration.Symbols.Declarations;
using CodeGeneration.Symbols.Declarators;
using CodeGeneration.Symbols.DerivedClasses;
using CodeGeneration.Symbols.ExceptionHandling;
using CodeGeneration.Symbols.Expressions;
using CodeGeneration.Symbols.LexicalConventions;
using CodeGeneration.Symbols.Overloading;
using CodeGeneration.Symbols.SpecialMemberFunctions;
using CodeGeneration.Symbols.Statements;
using CodeGeneration.Symbols.Templates;

namespace CodeGeneration.Symbols.Expressions {
    class PostfixExpressionFactory : ISymbolFactory {
        private readonly double  _optional;

        public PostfixExpressionFactory(double optional = 1) {
            _optional = optional;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() < context.Mutation.RandomDeletionProbability) {
                return new TerminalSymbol(@"/*tree corruption*/");
            }
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new PostfixExpression();
        }
    }

    class PostfixExpression : INonterminalSymbol {
        private bool _expanded;
        private readonly List<ISymbol> _symbols = new List<ISymbol>();
        private static readonly ISymbolFactory[] DefaultRule = {new TerminalSymbolFactory("0"), };

        private static readonly Tuple<ISymbolFactory[], double>[] Rules = {
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new LiteralFactory(), }, 0.35), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new IdExpressionFactory(), }, 0.425), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("this"), }, 0.45), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("("), new ExpressionFactory(), new TerminalSymbolFactory(")"), }, 0.475), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new LambdaExpressionFactory(), }, 0.5), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("["), new ExpressionFactory(), new TerminalSymbolFactory("]"), }, 0.525), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("["), new BracedInitListFactory(), new TerminalSymbolFactory("]"), }, 0.55), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("("), new ExpressionListFactory(optional: 0.5), new TerminalSymbolFactory(")"), }, 0.575), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new SimpleTypeSpecifierFactory(), new TerminalSymbolFactory("("), new ExpressionListFactory(optional: 0.5), new TerminalSymbolFactory(")"), }, 0.6), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TypenameSpecifierFactory(), new TerminalSymbolFactory("("), new ExpressionListFactory(optional: 0.5), new TerminalSymbolFactory(")"), }, 0.625), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new SimpleTypeSpecifierFactory(), new BracedInitListFactory(), }, 0.65), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TypenameSpecifierFactory(), new BracedInitListFactory(), }, 0.675), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("."), new TerminalSymbolFactory("template", optional: 0.5), new IdExpressionFactory(), }, 0.725), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("->"), new TerminalSymbolFactory("template", optional: 0.5), new IdExpressionFactory(), }, 0.75), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("."), new PseudoDestructorNameFactory(), }, 0.775), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("->"), new PseudoDestructorNameFactory(), }, 0.8), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("++"), }, 0.825), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new PostfixExpressionFactory(), new TerminalSymbolFactory("--"), }, 0.85), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("dynamic_cast"), new TerminalSymbolFactory("<"), new TypeIdFactory(), new TerminalSymbolFactory(">"), new TerminalSymbolFactory("("), new ExpressionFactory(), new TerminalSymbolFactory(")"), }, 0.875), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("static_cast"), new TerminalSymbolFactory("<"), new TypeIdFactory(), new TerminalSymbolFactory(">"), new TerminalSymbolFactory("("), new ExpressionFactory(), new TerminalSymbolFactory(")"), }, 0.9), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("reinterpret_cast"), new TerminalSymbolFactory("<"), new TypeIdFactory(), new TerminalSymbolFactory(">"), new TerminalSymbolFactory("("), new ExpressionFactory(), new TerminalSymbolFactory(")"), }, 0.925), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("const_cast"), new TerminalSymbolFactory("<"), new TypeIdFactory(), new TerminalSymbolFactory(">"), new TerminalSymbolFactory("("), new ExpressionFactory(), new TerminalSymbolFactory(")"), }, 0.95), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("type_id"), new TerminalSymbolFactory("("), new ExpressionFactory(), new TerminalSymbolFactory(")"), }, 0.975), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("typeid"), new TerminalSymbolFactory("("), new TypeIdFactory(), new TerminalSymbolFactory(")"), }, 1), 
        };

        public string Serialize() {
            return _expanded ? SymbolUtils.Serialize(_symbols) : "[" + GetType().Name + "]";
        }

        public List<ISymbol> Children() {
            return _symbols;
        }

        public Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context) {
            _symbols.Clear();
            _expanded = true;

            var chosenRule = DefaultRule;
            if (!timeToExit) {
                var selector = Utils.RandomDouble();
                foreach (Tuple<ISymbolFactory[], double> rule in Rules) {
                    if (selector < rule.Item2) {
                        chosenRule = rule.Item1;
                        break;
                    }
                }
            }

            return new Tuple<List<INonterminalSymbol>, bool>(SymbolUtils.Expand(chosenRule, _symbols, context), true);
        }
    }
}