using System;
using System.Collections.Generic;
using System.Linq;
using CodeGeneration.Symbols.BasicConcepts;
using CodeGeneration.Symbols.Classes;
using CodeGeneration.Symbols.Declarations;
using CodeGeneration.Symbols.Declarators;
using CodeGeneration.Symbols.DerivedClasses;
using CodeGeneration.Symbols.ExceptionHandling;
using CodeGeneration.Symbols.Expressions;
using CodeGeneration.Symbols.LexicalConventions;
using CodeGeneration.Symbols.Overloading;
using CodeGeneration.Symbols.SpecialMemberFunctions;
using CodeGeneration.Symbols.Statements;
using CodeGeneration.Symbols.Templates;

namespace CodeGeneration.Symbols.Templates {
    class InsideClassTemplateDeclarationFactory : ISymbolFactory {
        private readonly double  _optional;

        public InsideClassTemplateDeclarationFactory(double optional = 1) {
            _optional = optional;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() < context.Mutation.RandomDeletionProbability) {
                return new TerminalSymbol(@"/*tree corruption*/");
            }
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new InsideClassTemplateDeclaration();
        }
    }

    class InsideClassTemplateDeclaration : INonterminalSymbol {
        private bool _expanded;
        private readonly List<ISymbol> _symbols = new List<ISymbol>();
        private static readonly ISymbolFactory[] DefaultRule = {new TerminalSymbolFactory("template <int "), new IdentifierFactory(), new TerminalSymbolFactory(">"), new InsideClassSingleFunctionDeclarationFactory(), };

        private static readonly Tuple<ISymbolFactory[], double>[] Rules = {
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("template"), new TemplateParameterListWrapperFactory(), new InsideClassSingleFunctionDeclarationFactory(), new TerminalSymbolFactory(";"), }, 0.2), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("template"), new TemplateParameterListWrapperFactory(), new ClassSpecifierFactory(), new TerminalSymbolFactory(";"), }, 0.4), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("template"), new TemplateParameterListWrapperFactory(), new UsingDeclarationFactory(), new TerminalSymbolFactory(";"), }, 0.6), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("template"), new TemplateParameterListWrapperFactory(), new InsideClassTemplateDeclarationFactory(), }, 0.8), 
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new TerminalSymbolFactory("template"), new TemplateParameterListWrapperFactory(), new FriendDeclarationFactory(), }, 1.0), 
        };

        public string Serialize() {
            return _expanded ? SymbolUtils.Serialize(_symbols) : "[" + GetType().Name + "]";
        }

        public List<ISymbol> Children() {
            return _symbols;
        }

        public Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context) {
            _symbols.Clear();
            _expanded = true;

            var chosenRule = DefaultRule;
            if (!timeToExit) {
                var selector = Utils.RandomDouble();
                foreach (Tuple<ISymbolFactory[], double> rule in Rules) {
                    if (selector < rule.Item2) {
                        chosenRule = rule.Item1;
                        break;
                    }
                }
            }

            return new Tuple<List<INonterminalSymbol>, bool>(SymbolUtils.Expand(chosenRule, _symbols, context), true);
        }
    }
}