﻿using System;
using System.Collections.Generic;

namespace CodeGeneration.Symbols {
    public interface ISymbol {
        string Serialize();
        List<ISymbol> Children();
    }

    interface INonterminalSymbol : ISymbol {
        // list is for new symbols which are produced by the expansion, bool is for success (if node can't be expanded now, it's false)
        Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context);
    }

    class TerminalSymbol : ISymbol {
        private readonly string _value;

        public TerminalSymbol(string value) {
            _value = value;
        }

        public string Serialize() {
            return _value;
        }

        public List<ISymbol> Children() {
            return new List<ISymbol>();
        }
    }

    class EmptySymbol : ISymbol {
        public string Serialize() {
            return "";
        }

        public List<ISymbol> Children() {
            return new List<ISymbol>();
        }
    }
}
