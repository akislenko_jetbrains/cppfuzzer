﻿using System.Collections.Generic;

namespace CodeGeneration {
    class Mutation {
        public double RandomDeletionProbability { get; set; }
        public bool EllipsisEverywhere { get; set; }
    }

    class Context {
        private readonly string[] _identifierList = {"Id0", "Id1", "Id2", "Id3", "Id4"};
        private readonly Mutation _mutation;

        public Context(Mutation mutation) {
            _mutation = mutation;
        }

        public string[] Identifiers {
            get { return _identifierList; }
        }

        public Mutation Mutation { get { return _mutation; } }
    }
}
