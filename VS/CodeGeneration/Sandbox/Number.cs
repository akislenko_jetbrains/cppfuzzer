﻿using System.Collections.Generic;
using CodeGeneration.Symbols;

namespace CodeGeneration.Sandbox {
    class NumberFactory : ISymbolFactory {
        public ISymbol CreateInstance(Context context) {
            return new Number(context);
        }
    }

    class Number : ISymbol {
        private readonly int _value;

        public Number(Context context) {
//            do {
                _value = Utils.RandomInt(1000);
//            } while (context.Digits.Contains(_value.ToString()));
//            context.Digits.Add(_value.ToString());
        }

        public string Serialize() {
            return _value.ToString();
        }

        public List<ISymbol> Children() {
            return new List<ISymbol>();
        }

        public List<ISymbol> Expand() {
            return new List<ISymbol>();
        }
    }
}