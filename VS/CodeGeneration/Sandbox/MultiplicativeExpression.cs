﻿using System;
using System.Collections.Generic;
using CodeGeneration.Symbols;

namespace CodeGeneration.Sandbox {
    class MultiplicativeExpressionFactory : ISymbolFactory {
        private readonly bool _optional;

        public MultiplicativeExpressionFactory(bool optional = false) {
            _optional = optional;
        }

        public ISymbol CreateInstance(Context context) {
            if (_optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new MultiplicativeExpression();
        }
    }

    class MultiplicativeExpression : INonterminalSymbol {
        private bool _expanded;
        private readonly List<ISymbol> _symbols = new List<ISymbol>();
        private static readonly ISymbolFactory[] DefaultRule = {new NumberFactory(),};

        private static readonly Tuple<ISymbolFactory[], double>[] Rules = {
            new Tuple<ISymbolFactory[], double>(
                new ISymbolFactory[] {
                    new MultiplicativeExpressionFactory(), new MultiVariantTerminalSymbolFactory(new[] {"*", "/"}),
                    new NumberFactory()
                }, 0.9),
            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {new NumberFactory()}, 1),
        };

        public string Serialize() {
            return _expanded ? SymbolUtils.Serialize(_symbols) : "[" + GetType().Name + "]";
        }

        public List<ISymbol> Children() {
            return _symbols;
        }

        public Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context)
        {
            _symbols.Clear();
            _expanded = true;

            var chosenRule = DefaultRule;
            if (!timeToExit) {
                var selector = Utils.RandomDouble();
                foreach (Tuple<ISymbolFactory[], double> rule in Rules) {
                    if (selector < rule.Item2) {
                        chosenRule = rule.Item1;
                        break;
                    }
                }
//                chosenRule = Rules[Utils.RandomInt(Rules.Length)].Item1;
            }

            return new Tuple<List<INonterminalSymbol>, bool>(SymbolUtils.Expand(chosenRule, _symbols, context), true);
        }
    }
}