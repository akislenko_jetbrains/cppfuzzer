﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CodeGeneration;
using CodeGeneration.Symbols;

namespace Visualize {
    public partial class MainWindow : Window {
        private readonly Stack<ISymbol> _pathFromRoot = new Stack<ISymbol>();
        private ISymbol _current;
        private readonly Dictionary<object, ISymbol> _buttonToSymbol = new Dictionary<object, ISymbol>();

        const double ButtonHeight = 50;
        const double ButtonWidth = 200;
        const double DistanceBetweenButtons = 50;

        public MainWindow() {
            InitializeComponent();
        }

        private void CreateButtonForSymbol(ISymbol symbol, double left, double top) {
            var button = new Button {
                Content = symbol.GetType().Name,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Left,
                Height = ButtonHeight,
                Width = ButtonWidth,
                Margin = new Thickness(left, top, 0, 0)
            };
            button.Click += SymbolButton_OnClick;
            _buttonToSymbol[button] = symbol;
            GraphBox.Children.Add(button);
        }

        private void Update() {
            _buttonToSymbol.Clear();
            GraphBox.Children.Clear();

            GraphBox.Width = _current.Children().Count * (DistanceBetweenButtons + ButtonWidth) + DistanceBetweenButtons;
            if (GraphBox.Width < ScrollField.ActualWidth) {
                GraphBox.Width = ScrollField.ActualWidth;
            }

            if (_pathFromRoot.Count > 0) {
                CreateButtonForSymbol(_pathFromRoot.Peek(), (GraphBox.Width - ButtonWidth) / 2,
                    GraphBox.ActualHeight / 4 - ButtonHeight / 2);
            }
            CreateButtonForSymbol(_current, (GraphBox.Width - ButtonWidth) / 2,
                (GraphBox.ActualHeight - ButtonHeight) / 2);
            
            for (int i = 0; i < _current.Children().Count; ++i) {
                CreateButtonForSymbol(_current.Children()[i],
                    i * (DistanceBetweenButtons + ButtonWidth) + DistanceBetweenButtons,
                    GraphBox.ActualHeight * 3 / 4 - ButtonHeight / 2);
            }

            ScrollField.ScrollToHorizontalOffset((GraphBox.Width - GraphBox.ActualWidth) / 2);
            CodeBox.Text = _current.Serialize();
        }

        private void SymbolButton_OnClick(object sender, RoutedEventArgs routedEventArgs) {
            var chosenSymbol = _buttonToSymbol[sender];

            if (chosenSymbol == _current) {
                return;
            }
            if (_pathFromRoot.Count > 0 && chosenSymbol == _pathFromRoot.Peek()) {
                _current = _pathFromRoot.Pop();
            } else {
                _pathFromRoot.Push(_current);
                _current = chosenSymbol;
            }
            Update();
        }

        private ISymbol GenerateTree() {
            return Generator.GenerateTree();
        }

        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Escape) {
                Close();
            }
            if (e.Key == Key.Space) {
                _current = GenerateTree();
                Update();
            }
            if (e.Key == Key.C && e.KeyboardDevice.IsKeyDown(Key.LeftCtrl)) {
                Clipboard.SetText(CodeBox.Text);
            }
        }

        private void MainWindow_OnSizeChanged(object sender, SizeChangedEventArgs e) {
            if (_current == null) {
                _current = GenerateTree();
            }
            Update();
        }
    }
}
