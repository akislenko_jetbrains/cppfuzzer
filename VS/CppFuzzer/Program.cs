﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CodeGeneration;

namespace CppFuzzer {
    public static class Program {
        static void Main(string[] args) {
            const string vsTestPath = @"../../../../test/vs/vs/Generated";
            const string clTestPath = @"../../../../test/clion/Generated";
            const bool vsNeedExport = true;
            const bool clNeedExport = true;

            for (int i = 0; i < 100; ++i) {
                Console.WriteLine(i);
                var root = Generator.GenerateTree();
//                Console.WriteLine(root.Serialize());

                if (clNeedExport) {
                    using (var writer = new StreamWriter(clTestPath + @"\Code" + i + ".cpp")) {
                        writer.Write(root.Serialize());
                    }
                }
                if (vsNeedExport) {
                    using (var writer = new StreamWriter(vsTestPath + @"\Code" + i + ".cpp")) {
                        writer.Write(root.Serialize());
                    }
                }
            }
        }
    }
}