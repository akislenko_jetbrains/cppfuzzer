from bs4 import BeautifulSoup
from bs4 import Tag
from bs4 import NavigableString
import bs4

# parse grammar.txt
f = open('grammar.xml')
s = f.read()
f.close()
soup = BeautifulSoup(s, 'html.parser')

symbols = soup.findAll('symbol')
grammar = {}

for symbol in symbols:
    tokens = set()
    for nonterminal in symbol.findAll('n'):
        tokens.add(nonterminal['name'])

    grammar[symbol['name']] = tokens

# generate graph
vertices = grammar.keys()

graph = Tag(name='graph')
graph['edgedefault'] = 'directed'
graph['id'] = 'G'
graph.append(Tag(name='data', attrs={'key': 'd0'}))
i = 0
ruleToId = {}
for rule in grammar:
    node = Tag(name='node', attrs={'id': 'n' + str(i)})
    ruleToId[rule] = 'n' + str(i)
    node.append(Tag(name='data', attrs={'key': 'd5'}))
    data = Tag(name='data', attrs={'key': 'd6'})
    shape = Tag(name='ShapeNode', namespace='y')
    shape.append(Tag(name='y:Geometry', attrs={'height': '50', 'width': '100', 'x': '0', 'y': '0'}))
    shape.append(Tag(name='y:Fill', attrs={'Color': '#FFCC00', 'transparent': 'false'}))
    shape.append(Tag(name='y:BorderStyle', attrs={'Color': '#000000', 'type': 'line', 'width': '1.0'}))
    label = Tag(name='y:NodeLabel',
                attrs={'alignment': 'center', 'autoSizePolicy': 'content', 'fontFamily': 'Dialog', 'fontSize': '12',
                       'fontStyle': 'plain', 'hasBackgroundColor': 'false', 'hasLineColor': 'false', 'height': '20',
                       'modelName': 'custom', 'textColor': '#000000', 'visible': 'true', 'width': '90'})
    label.append(NavigableString(rule))
    shape.append(label)
    shape.append(Tag(name='y:Shape', attrs={'type': 'rectangle' if len(grammar[rule]) > 0 else 'ellipse', 'transparent': 'false'}))

    data.append(shape)
    node.append(data)
    graph.append(node)
    i += 1

i = 0
for src in grammar:
    for dst in grammar[src]:
        edge = Tag(name='edge', attrs={'id': 'e' + str(i), 'source': ruleToId[src], 'target': ruleToId[dst]})
        edge.append(Tag(name='data', attrs={'key': 'd9'}))
        data = Tag(name='data', attrs={'key': 'd10'})
        line = Tag(name='y:PolyLineEdge')
        line.append(Tag(name='y:Path', attrs={'sx': '0', 'sy': '0', 'tx': '0', 'ty': '0'}))
        line.append(Tag(name='y:LineStyle', attrs={'color': '#000000', 'type': 'line', 'width': '1'}))
        line.append(Tag(name='y:Arrows', attrs={'source': 'none', 'target': 'standard'}))
        line.append(Tag(name='y:BendStyle', attrs={'smoothed': 'false'}))

        data.append(line)
        edge.append(data)
        graph.append(edge)
        i += 1

outputFile = open('generated.graphml', 'w')
outputFile.write('''<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:java="http://www.yworks.com/xml/yfiles-common/1.0/java" xmlns:sys="http://www.yworks.com/xml/yfiles-common/markup/primitives/2.0" xmlns:x="http://www.yworks.com/xml/yfiles-common/markup/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <!--Created by yEd 3.15.0.2-->
  <key attr.name="Description" attr.type="string" for="graph" id="d0"/>
  <key for="port" id="d1" yfiles.type="portgraphics"/>
  <key for="port" id="d2" yfiles.type="portgeometry"/>
  <key for="port" id="d3" yfiles.type="portuserdata"/>
  <key attr.name="url" attr.type="string" for="node" id="d4"/>
  <key attr.name="description" attr.type="string" for="node" id="d5"/>
  <key for="node" id="d6" yfiles.type="nodegraphics"/>
  <key for="graphml" id="d7" yfiles.type="resources"/>
  <key attr.name="url" attr.type="string" for="edge" id="d8"/>
  <key attr.name="description" attr.type="string" for="edge" id="d9"/>
  <key for="edge" id="d10" yfiles.type="edgegraphics"/>
  ''' + str(graph) + '''
  <data key="d7">
    <y:Resources/>
  </data>
</graphml>''')

