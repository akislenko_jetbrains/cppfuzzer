from bs4 import BeautifulSoup
from os import mkdir
from os import path
from sys import argv


def validate(xml):
    errors = []
    symbols = [symbol['name'] for symbol in xml.findAll('symbol')]
    for rule in xml.findAll('n'):
        if not (rule['name'] in symbols):
            errors.append(rule)

    symbols = xml.findAll('symbol')
    for symbol in symbols:
        rules = symbol.findAll('rule')

        # check if there are mistakes with attribute id (more or less than one rule with the certain id)
        ids = set()
        for rule in rules:
            if rule['id'] in ids:
                errors.append(symbol)
            ids.add(int(rule['id']))
        for id in ids:
            if id < 0 or id >= len(ids):
                errors.append(symbol)

        # check if there are mistakes with attribute p (some rule for current symbol contains probability,
        # when another one doesn't)
        if 0 < len([rule for rule in rules if rule.has_attr('p')]) < len(rules):
            errors.append(symbol)

    return len(errors) == 0


def generateFactoriesCode(tag):
    result = ''

    rules = tag.findAll('rule')
    for i in range(len(rules)):
        rule = tag.find('rule', attrs={'id': str(i)})

        result += '\n            new Tuple<ISymbolFactory[], double>(new ISymbolFactory[] {'

        ruleSymbols = rule.findAll(recursive='false')
        for child in ruleSymbols:
            if child.name == 'n':
                opt = '' if not child.has_attr('opt') else 'optional: ' + child['opt']
                result += 'new ' + child['name'] + 'Factory(' + opt + ')'
            elif child.name == 't':
                opt = '' if not child.has_attr('opt') else ', optional: ' + child['opt']
                leftIndent = '' if not child.has_attr('l') else ', leftIndent: ' + child['l']
                rightIndent = '' if not child.has_attr('r') else ', rightIndent: ' + child['r']
                result += 'new TerminalSymbolFactory(' + '"' + child['value'] + '"' + opt + leftIndent + rightIndent + ')'
            elif child.name == 'o':
                opt = '' if not child.has_attr('opt') else ', optional: ' + child['opt']
                leftIndent = '' if not child.has_attr('l') else ', leftIndent: ' + child['l']
                rightIndent = '' if not child.has_attr('r') else ', rightIndent: ' + child['r']
                result += 'new MultiVariantTerminalSymbolFactory(new[] {'
                collection = [token for token in child['values'].split(' ') if token != '']
                for item in collection:
                    result += '"' + item + '", '
                result += '}' + opt + leftIndent + rightIndent + ')'

            result += ', '

        probability = (i + 1) / len(rules)
        if rule.has_attr('p'):
            probability = rule['p']

        result += '}, ' + str(probability) + '), '

    return result


def generateDefaultRuleCode(tag):
    result = ''

    rule = symbol.find('defaultrule')
    for child in rule.findAll(recursive='false'):
        if child.name == 'n':
            opt = '' if not child.has_attr('opt') else 'optional: ' + child['opt']
            result += 'new ' + child['name'] + 'Factory(' + opt + ')'
        elif child.name == 't':
            opt = '' if not child.has_attr('opt') else ', optional: ' + child['opt']
            leftIndent = '' if not child.has_attr('l') else ', leftIndent: ' + child['l']
            rightIndent = '' if not child.has_attr('r') else ', rightIndent: ' + child['r']
            result += 'new TerminalSymbolFactory(' + '"' + child['value'] + '"' + opt + leftIndent + rightIndent + ')'
        elif child.name == 'o':
            opt = '' if not child.has_attr('opt') else ', optional: ' + child['opt']
            leftIndent = '' if not child.has_attr('l') else ', leftIndent: ' + child['l']
            rightIndent = '' if not child.has_attr('r') else ', rightIndent: ' + child['r']
            result += 'new MultiVariantTerminalSymbolFactory(new[] {'
            collection = [token for token in child['values'].split(' ') if token != '']
            for item in collection:
                result += '"' + item + '", '
            result += '}' + opt + leftIndent + rightIndent + ')'

        result += ', '

    return result

outputPath = argv[1]

f = open('grammar.xml')
s = f.read()
f.close()
soup = BeautifulSoup(s, 'html.parser')

if not validate(soup):
    print('validation failed')
    exit()

namespaces = soup.findAll('namespace')
for namespace in namespaces:
    namespaceName = namespace['name']
    dirName = outputPath + '/' + namespaceName + '/'
    if not path.exists(dirName):
        mkdir(dirName)
    for symbol in namespace.findAll('symbol'):
        if symbol.has_attr('custom') and symbol['custom'] == 'true':
            continue
        className = symbol['name']

        defaultRule = generateDefaultRuleCode(symbol)
        factoriesCode = generateFactoriesCode(symbol)

        srcFile = open(dirName + className + '.cs', 'w')
        srcFile.write(
            '''using System;
using System.Collections.Generic;
using System.Linq;
using CodeGeneration.Symbols.BasicConcepts;
using CodeGeneration.Symbols.Classes;
using CodeGeneration.Symbols.Declarations;
using CodeGeneration.Symbols.Declarators;
using CodeGeneration.Symbols.DerivedClasses;
using CodeGeneration.Symbols.ExceptionHandling;
using CodeGeneration.Symbols.Expressions;
using CodeGeneration.Symbols.LexicalConventions;
using CodeGeneration.Symbols.Overloading;
using CodeGeneration.Symbols.SpecialMemberFunctions;
using CodeGeneration.Symbols.Statements;
using CodeGeneration.Symbols.Templates;

namespace CodeGeneration.Symbols.''' + namespaceName + ''' {
    class ''' + className + '''Factory : ISymbolFactory {
        private readonly double  _optional;

        public ''' + className + '''Factory(double optional = 1) {
            _optional = optional;
        }

        public ISymbol CreateInstance(Context context) {
            if (Utils.RandomDouble() < context.Mutation.RandomDeletionProbability) {
                return new TerminalSymbol(@"/*tree corruption*/");
            }
            if (Utils.RandomDouble() > _optional && Utils.RandomBool()) {
                return new EmptySymbol();
            }
            return new ''' + className + '''();
        }
    }

    class ''' + className + ''' : INonterminalSymbol {
        private bool _expanded;
        private readonly List<ISymbol> _symbols = new List<ISymbol>();
        private static readonly ISymbolFactory[] DefaultRule = {''' + defaultRule + '''};

        private static readonly Tuple<ISymbolFactory[], double>[] Rules = {''' + factoriesCode + '''
        };

        public string Serialize() {
            return _expanded ? SymbolUtils.Serialize(_symbols) : "[" + GetType().Name + "]";
        }

        public List<ISymbol> Children() {
            return _symbols;
        }

        public Tuple<List<INonterminalSymbol>, bool> Expand(bool timeToExit, Context context) {
            _symbols.Clear();
            _expanded = true;

            var chosenRule = DefaultRule;
            if (!timeToExit) {
                var selector = Utils.RandomDouble();
                foreach (Tuple<ISymbolFactory[], double> rule in Rules) {
                    if (selector < rule.Item2) {
                        chosenRule = rule.Item1;
                        break;
                    }
                }
            }

            return new Tuple<List<INonterminalSymbol>, bool>(SymbolUtils.Expand(chosenRule, _symbols, context), true);
        }
    }
}''')
        srcFile.close()
